const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const http = require('http');
const routes = require('./routes');
const { setupWebsocket } = require('./websocket');

const app = express();
const server = http.Server(app);

setupWebsocket(server);

mongoose.connect('mongodb://root:bancodedados@mongo:27017/my_DB?authSource=admin',{
    useNewUrlParser: true,
    useUnifiedTopology: true,
});

app.use(cors({ origin: '*' }));// fix adress localhost:3000 dont work in docker
app.use(express.json());
app.use(routes);

server.listen(3333);